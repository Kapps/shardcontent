module ShardContent.ContentImporter;
public import ShardTools.StreamReader;


/// The base class for an importer used to load an asset.
/// Params:
///		T = The type of the asset to import.
abstract class ContentImporter(T) {

	/// Indicates the results of an asset import.
	/// Params:
	/// 	T = The type of asset being imported.
	public class ImportResult {

		this(T AssetImported, bool CanReload) {
			this._AssetImported = AssetImported;
			this._CanReload = CanReload;
		}
		
		/// Gets the asset that was imported.
		@property public T AssetImported() {
			return _AssetImported;
		}

		/// Indicates whether the asset can be reloaded.
		@property bool CanReload() const {
			return _CanReload;			
		}

		private T _AssetImported;
		private bool _CanReload;
	}

	/// Imports an asset from the specified Data.
	/// Params:
	///		Data = The data to import an asset from.
	///	Returns: 
	/// 	An ImportResult containing the newly created instance of T, and whether this newly created instance can be reloaded when changed, using ReloadAsset.
	public ImportResult ImportAsset(StreamReader Data);

	/// Reloads a previously loaded asset with the new data.
	/// It is up to the implementor to notify the asset of the newly changed data, and handle the actual updating.
	/// The result should not change the reference, as multiple objects may reference the same asset. As such, this method returns void.
	/// If ImportAsset always has CanReload be false, this method will never be called.
	/// It is important to note that assets will only be reloaded when imported directly be the ContentLoader.
	/// That means that if an asset chains another asset's importer, the chained asset will not be imported.
	/// It is guaranteed that the instance of the importer that calls ImportAsset is the same one that calls ReloadAsset for the same asset.
	/// Params:
	/// 	Asset = The asset to reload.
	/// 	Data = The data to import the new version of the asset from.
	void ReloadAsset(T Asset, StreamReader Data);

	alias T AssetType;
}