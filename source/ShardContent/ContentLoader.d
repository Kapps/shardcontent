module ShardContent.ContentLoader;
private import std.conv;
private import ShardTools.ExceptionTools;
public import ShardContent.ContentWatcher;

public import ShardContent.ContentImporter;

private import std.zlib;
private import std.exception;
private import std.path;
private import std.file;
private import ShardTools.PathTools;
private import ShardTools.IDisposable;
private import ShardTools.Disposable;
private import std.string;
private import ShardTools.Logger;
import std.array : replace;

mixin(MakeException("FileLoadException", "An error occurred when loading a file."));
mixin(MakeException("InvalidDataException", "The asset contained invalid data."));
mixin(MakeException("InternalErrorException", "An internal error has occurred."));


/// A class used to load a single Content asset.
class ContentLoader : Disposable {
public:
	///Initializes a new instance of the ContentLoader object.
	this() {
		_Watcher = new ContentWatcher(this);
	}
	
	shared static this() {
		_Default = new ContentLoader();
	}

	/// Gets a global ContentLoader instance to use for loading persistent assets.
	@property static ContentLoader Default() {
		return _Default;
	}
	
	/// Loads the asset located at the specified FilePath.
	/// Params:
	///		Importer = The type of the Importer to use to load the asset.
	///		AssetType = The type of the Asset after being fully loaded.
	///		FilePath = The path to the file to load.
	Importer.AssetType Load(Importer)(in char[] FilePath) {		
		char[] FixedPath = GetFixedPath(FilePath);				
		enforceEx!FileLoadException(exists(FixedPath), "The asset to load at \'" ~ to!string(FixedPath) ~ "\' could not be found.");
		Object* asset = (FixedPath in Assets);
		if(asset !is null)
			return cast(Importer.AssetType)*asset;
		return LoadAsset!(Importer, Importer.AssetType)(FixedPath);				
	}
	
	/// Removes the asset contained at the specified path.
	/// If the asset contains IDisposable and Dispose is true, IDisposable.Dispose is called on it.	
	/// Params:
	///		FilePath = The path to the asset to remove.
	///		Dispose = Whether to dispose of the asset after removing it.
	/// Returns: 
	///		Whether an asset was removed.
	bool Remove(in char[] FilePath, bool Dispose = true) {
		// TODO: Decide whether the asset should be deleted.
		char[] path = GetFixedPath(FilePath);
		Object* asset = (path in Assets);
		if(asset is null)
			return false;
		Assets.remove(path);
		if(Dispose)
			DisposeAsset(*asset);		
		return true;
	}

	/// Removes all assets contained by this ContentLoader, disposing of them if possible.
	void Unload() {
		foreach(Object asset; Assets.values)			
			DisposeAsset(asset);		
		foreach(key; Assets.keys)
			Assets.remove(key);
	}

	/// Gets a ContentWatcher for this particular ContentLoader, used to scan for external changes to content files.
	/// By default, the watcher is disabled.
	@property ContentWatcher Watcher() {
		return _Watcher;
	}

protected:
	override void OnDispose() {
		Unload();
		super.OnDispose();		
	}

private:	
	void DisposeAsset(Object asset) {
		IDisposable disposable = cast(IDisposable)asset;
		if(disposable)
			disposable.Dispose();
	}

	AssetType LoadAsset(Importer : ContentImporter!(AssetType), AssetType)(in char[] FixedPath) {
		StreamReader Reader = CreateReader(FixedPath);
		ContentImporter!(AssetType) Importer = new Importer();
		Importer.ImportResult Result = Importer.ImportAsset(Reader);
		AssetType Asset = Result.AssetImported;
		Assets[FixedPath] = Asset;
		if(Result.CanReload)
			Watcher.AddWatchedAsset(Asset, FixedPath, &ReloadAsset, cast(ContentWatcher.ImporterReloadDelegate)&Importer.ReloadAsset);
		return Asset;
	}

	void ReloadAsset(Object Asset, string FixedPath, ContentWatcher.ImporterReloadDelegate ReloadDelegate) {
		StreamReader Reader = CreateReader(FixedPath);
		ReloadDelegate(Asset, Reader);
	}

	private StreamReader CreateReader(in char[] FixedPath) {
		uint UncompressedSize;
		ubyte[] CompressedData = GetCompressedData(FixedPath, UncompressedSize);
		ubyte[] UncompressedData = GetUncompressedData(CompressedData, UncompressedSize);
		//string ImportType = ReadImporterType(UncompressedData);
		//ContentImporter!(AssetType) Importer = cast(ContentImporter!(AssetType))Object.factory(ImportType);				
		StreamReader Reader = new StreamReader(UncompressedData, false);
		return Reader;
	}

	/+string ReadImporterType(ubyte[] UncompressedData) {
		uint NameLength = *(cast(uint*)UncompressedData[0..4]);
		string Name = cast(string)(UncompressedData[4..NameLength + 4]).idup;
		return Name;
	}+/

	ubyte[] GetUncompressedData(ubyte[] CompressedData, uint UncompressedSize) {
		ubyte[] Result = cast(ubyte[])uncompress(cast(void[])CompressedData, UncompressedSize);
		return Result;
	}

	ubyte[] GetCompressedData(in char[] FixedPath, out uint UncompressedSize) {
		ubyte[] FileContents = cast(ubyte[])read(FixedPath);
		UncompressedSize = *(FileContents.ptr);
		ubyte[] CompressedData = FileContents[4 .. $];
		return CompressedData;
	}
	
	static package char[] GetFixedPath(in char[] FilePath) {
		char[] path = cast(char[])PathTools.MakeAbsolute(FilePath);
		if(path.length < 4 || path[$-4..$] != ".saf")
			path = path ~ ".saf";		
		version(Windows) {
			path = path.replace("/", "\\");
			path = path.toLower();
		} else {
			path = path.replace("\\", "/");
		}
		return path.dup;
	}

	Object[char[]] Assets;
	ContentWatcher _Watcher;
	static __gshared ContentLoader _Default;	
}