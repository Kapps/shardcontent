﻿module ShardContent.ContentWatcher;
private import std.conv;
private import ShardTools.WeakReference;
private import std.exception;
private import ShardContent.ContentLoader;
import core.time;
import core.thread;
import std.file;
import std.datetime;


/// Watches for external changes to Content files, reloading Content as necessary.
/// Only assets with the IReloadable interface may be reloaded.
class ContentWatcher  {

public:
	/// Initializes a new instance of the ContentWatcher object.
	this(ContentLoader Loader) {
		this._Loader = Loader;
	}

	/// Gets the ContentLoader owning this ContentWatcher.
	@property ContentLoader Loader() {
		return _Loader;
	}

	/// Indicates whether this ContentLoader is currently watching for changes.
	@property bool IsWatching() const {
		return _IsWatching;
	}
	
	/// Begins watching for any external changes to content files, reloading when they occur.
	void StartWatching() {
		enforce(!IsWatching, "Unable to start watching for changes when already watching.");
		_IsWatching = true;
		Thread t = new Thread(&BeginWatchLoop);
		t.isDaemon = true;
		t.start();
	}

	/// Stops watching for any external changes.
	void StopWatching() {
		enforce(IsWatching, "Unable to stop watching for changes when already not watching.");
		_IsWatching = false;
	}

	package alias void delegate(Object Asset, string FixedPath, ImporterReloadDelegate ReloadDelegate) ReloadAssetCallback;
	package alias void delegate(Object Asset, StreamReader Reader) ImporterReloadDelegate;

	/// Adds the given asset to be watched for changes.
	/// Params:
	/// 	AssetPath = The path to the data the asset was loaded from.
	/// 	Asset = The asset to watch for changes.
	/// 	Callback = The callback to invoke when the asset is reloaded.
	/// 	ReloadDelegate = A delegate containing the ReloadAsset method of the importer that loaded the asset.
	package void AddWatchedAsset(Object Asset, in char[] AssetPath, ReloadAssetCallback Callback, ImporterReloadDelegate ReloadDelegate) {
		string FixedPath = ContentLoader.GetFixedPath(AssetPath).idup;				
		enforce((FixedPath in _WatchedAssets) is null, "The given asset was already added.");		
		WatchedAsset Holder;
		Holder.Asset = new WeakReference!Object(cast(Object)Asset, &AssetDeleted);
		Holder.AssetPath = FixedPath;
		Holder.LastModified = Clock.currTime();
		Holder.Callback = Callback;
		Holder.ReloadDelegate = ReloadDelegate;
		Holder.LastSize = getSize(FixedPath);
		_WatchedAssets[FixedPath] = Holder;
	}
	
	/// Stops tracking the given asset for changes.
	/// Params:
	/// 	Asset = The asset to stop tracking.
	package void StopTrackingAsset(Object Asset) {
		bool Found = false;
		foreach(Key, Value; _WatchedAssets) {			
			if(Value.Asset.Pointer == cast(void*)Asset) {
				_WatchedAssets.remove(Key);
				Found = true;
				break;
			}
		}
		enforce(Found, "Unable to stop tracking an asset that was not tracked.");
	}	

private:
	WatchedAsset[string] _WatchedAssets;
	bool _IsWatching = false;
	ContentLoader _Loader;	

	struct WatchedAsset {
		WeakReference!Object Asset;
		string AssetPath;
		SysTime LastModified;
		long LastSize; // We have no way of figuring out if a file was modified by being renamed / copied / etc. So, this works as a hack. 
		ReloadAssetCallback Callback;
		ImporterReloadDelegate ReloadDelegate;
	}
	 
	void AssetDeleted(WeakReference!Object Reference) {
		Object Asset = cast(Object)Reference.Pointer;
		StopTrackingAsset(Asset);				
	}

	void BeginWatchLoop() {
		while(_IsWatching) {
			synchronized(this) {
				foreach(string Path, WatchedAsset Asset; _WatchedAssets) {
					if(!exists(Path))
						continue; // TODO: Warn somehow?					
					//SysTime CurrMod = timeLastModified(Path);
					SysTime Created, Accessed, Modified;			
					version(Windows)		
						getTimesWin(Path, Created, Accessed, Modified);
					else version(Posix) {
						getTimes(Path, Accessed, Modified);
						Created = Accessed;
					}
					long NewSize = getSize(Path);					
					if(Created > Asset.LastModified || Modified > Asset.LastModified || NewSize != Asset.LastSize) {						
						ReloadAsset(Asset);
						_WatchedAssets[Path] = Asset;
					}					
				}
			}
			Thread.sleep(dur!"msecs"(500));
		}
	}

	void ReloadAsset(ref WatchedAsset Asset) {		
		synchronized(this) {
			Asset.LastModified = Clock.currTime();
			Asset.LastSize = getSize(Asset.AssetPath);
			Asset.Callback(Asset.Asset.Value, Asset.AssetPath, Asset.ReloadDelegate);		
		}
	}
}