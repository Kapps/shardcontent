﻿module ShardContent.TextAsset;
private import ShardTools.Disposable;
public import ShardContent.TextImporter;

/// Represents a single, reloadable, text asset.
class TextAsset : Disposable {	

public:
	this(string TextValue) {
		this._Text = TextValue;
	}

	/// Gets the text represented by this asset.
	/// If the asset is reloaded, this may contain different values.
	@property string Text() const {
		return _Text;
	}		

	/// Occurs when this object has Dispose called when it was not already disposed.
	protected override void OnDispose() {

	}

	package void SetText(string Value) {
		_Text = Value;
	}

private:
	string _Text;
}