﻿module ShardContent.TextImporter;
public import ShardContent.ContentImporter;
public import ShardContent.TextAsset;

class TextImporter : ContentImporter!(TextAsset) {

public:
	/// Initializes a new instance of the TextImporter object.
	this() {
		
	}
	
	/// Imports an asset from the specified Data.
	/// Params:
	///		Data = The data to import an asset from.
	/// Returns: 
	///		An ImportResult containing the newly created instance of T, and whether this newly created instance can be reloaded when changed, using ReloadAsset.
	override ImportResult ImportAsset(StreamReader Data) {	
		string Text = ReadText(Data);
		TextAsset Asset = new TextAsset(Text);
		return new ImportResult(Asset, true);		
	}	

	private string ReadText(StreamReader Data) {
		char[] Array = cast(char[])Data.RemainingData[4..$];
		return Array.idup;
	}

	/// Reloads a previously loaded asset with the new data.
	/// It is up to the implementor to notify the asset of the newly changed data, and handle the actual updating.
	/// The result should not change the reference, as multiple objects may reference the same asset. As such, this method returns void.
	/// If ImportAsset always has CanReload be false, this method will never be called.
	/// Params:
	///		Asset = The asset to reload.
	///		Data = The data to import the new version of the asset from.
	override void ReloadAsset(TextAsset Current, StreamReader Data) {
		string Text = ReadText(Data);
		Current.SetText(Text);
	}
	
private:	
}